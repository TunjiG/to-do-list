<?php

namespace spec\App\Service;

use App\Service\ToDoService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use App\Factory\ModelFactoryInterface;
use App\Factory\EntityFactoryInterface;
use App\Repository\ToDoRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Exception\ToDoException;
use App\Model\ToDo as ToDoModel;
use App\Entity\ToDo as ToDoEntity;
use DateTime;
use Exception;
use App\Model\ToDoCollection;

class ToDoServiceSpec extends ObjectBehavior
{
    function let(
        EntityFactoryInterface $entityFactory,
        ModelFactoryInterface $modelFactory,
        ToDoRepository $toDoRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->beConstructedWith(
            $entityFactory,
            $modelFactory,
            $toDoRepository,
            $entityManager
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ToDoService::class);
    }

    function it_throws_an_exception_if_item_could_not_be_found_during_get(
        ToDoRepository $toDoRepository
    ) {
        $toDoRepository->find(1)->willReturn(null);

        $this->shouldThrow(ToDoException::class)->duringGet(1);
    }

    function it_sucessfully_gets_an_item(
        ModelFactoryInterface $modelFactory,
        ToDoEntity $entity,
        ToDoRepository $toDoRepository
    ) {
        $toDoRepository->find(1)->willReturn($entity);

        $modelFactory->createToDo($entity)->shouldBeCalled();

        $this->get(1);
    }

    function it_throws_an_exception_in_case_of_database_error_during_create(
        EntityManagerInterface $entityManager,
        EntityFactoryInterface $entityFactory,
        ToDoModel $model,
        ToDoEntity $entity,
        ToDoRepository $toDoRepository
    ) {
        $model->dueDate = new DateTIme('2018-05-05');
        $model->description = 'An updated description';

        $entityManager->flush()->willThrow(new Exception());

        $this->shouldThrow(ToDoException::class)->duringUpdate($model);
    }

    function it_throws_an_exception_if_item_could_not_be_found_during_update(
        ToDoModel $model,
        ToDoRepository $toDoRepository
    ) {
        $model->id = 2;

        $toDoRepository->find($model->id)->willReturn(null);

        $this->shouldThrow(ToDoException::class)->duringUpdate($model);
    }

    function it_sucessfully_updates_an_item(
        EntityFactoryInterface $entityFactory,
        ToDoModel $model,
        ToDoEntity $entity,
        ToDoRepository $toDoRepository
    ) {
        $model->dueDate = new DateTIme('2018-05-05');
        $model->description = 'An updated description';
        $model->id = 1;

        $toDoRepository->find($model->id)->willReturn($entity);

        $entityFactory->updateToDo($model, $entity)->shouldBeCalled();

        $this->update($model);
    }

    function it_sucessfully_lists_items(
        ModelFactoryInterface $modelFactory,
        ToDoEntity $entity,
        ToDoRepository $toDoRepository
    ) {
        $entity->getId()->willReturn(1);
        $entity->getDescription()->willReturn('A description');
        $entity->getDueAt()->willReturn(new DateTIme('2018-05-03'));
        $entity->getUpdatedAt()->willReturn(new DateTime('2018-04-30'));

        $toDoRepository->countUndeleted()->willReturn(1);

        $toDoRepository
        ->findPaginatedUndeleted(ToDoService::PAGE_LIMIT, 0)
        ->shouldBeCalled()
        ->willReturn([$entity]);

        $modelFactory->createToDo($entity)->shouldBeCalled();

        $collection = $this->list(1);

        $collection->shouldHaveType(ToDoCollection::class);
        $collection->total->shouldBe(1);
        $collection->page->shouldBe(1);
    }

    function it_sucessfully_lists_items_and_normalises_page_according_to_total_pages(
        ModelFactoryInterface $modelFactory,
        ToDoEntity $entity,
        ToDoRepository $toDoRepository
    ) {
        $entity->getId()->willReturn(1);
        $entity->getDescription()->willReturn('A description');
        $entity->getDueAt()->willReturn(new DateTIme('2018-05-03'));
        $entity->getUpdatedAt()->willReturn(new DateTime('2018-04-30'));

        $toDoRepository->countUndeleted()->willReturn(1);

        $toDoRepository
        ->findPaginatedUndeleted(ToDoService::PAGE_LIMIT, 0)
        ->shouldBeCalled()
        ->willReturn([$entity]);

        $modelFactory->createToDo($entity)->shouldBeCalled();

        $collection = $this->list(100);

        $collection->shouldHaveType(ToDoCollection::class);
        $collection->total->shouldBe(1);
        $collection->page->shouldBeLike(1);
    }

    function it_throws_an_exception_if_item_could_not_be_found_during_delete(
        ToDoRepository $toDoRepository
    ) {
        $toDoRepository->find(3)->willReturn(null);

        $this->shouldThrow(ToDoException::class)->duringDelete(3);
    }

    // This doesn't seem to work
    // function it_sucessfully_deletes_an_item(
    //     EntityManagerInterface $entityManager,
    //     ToDoEntity $entity,
    //     ToDoRepository $toDoRepository
    // ) {
    //     $toDoRepository->find(3)->willReturn($entity);
    //
    //     $entityManager->remove($entity)->shouldBeCalled();
    //
    //     $this->delete(3);
    // }
}
