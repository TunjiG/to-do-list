<?php

namespace spec\App\Factory;

use App\Factory\EntityFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use App\Entity\ToDo as ToDoEntity;
use App\Model\ToDo as ToDoModel;
use DateTIme;

class EntityFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(EntityFactory::class);
    }

    function it_creates_entity(ToDoModel $model)
    {
        $model->dueDate = new DateTIme('2018-05-03');
        $model->description = 'A description';

        $entity = $this->createToDo($model);

        $entity->getDescription()->shouldBe($model->description);
        $entity->getDueAt()->shouldHaveType(DateTIme::class);
        $entity->getDueAt()->format(DateTime::W3C)->shouldBe($model->dueDate->format(DateTime::W3C));
        $entity->getCreatedAt()->shouldHaveType(DateTIme::class);
        $entity->getUpdatedAt()->shouldHaveType(DateTIme::class);
        $entity->getUpdatedAt()->format(DateTime::W3C)->shouldBe($entity->getCreatedAt()->format(DateTime::W3C));
    }

    function it_updates_entity(ToDoModel $model, ToDoEntity $entity)
    {
        $model->dueDate = new DateTIme('2018-05-05');
        $model->description = 'An updated description';

        $entity->setUpdatedAt(Argument::type(DateTime::class))->shouldBeCalled()->willReturn($entity);
        $entity->setDueAt($model->dueDate)->shouldBeCalled()->willReturn($entity);
        $entity->setDescription($model->description)->shouldBeCalled()->willReturn($entity);

        $entity = $this->updateToDo($model, $entity);
    }
}
