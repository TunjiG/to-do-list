<?php

namespace spec\App\Factory;

use App\Factory\ModelFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use App\Entity\ToDo;
use DateTIme;

class ModelFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ModelFactory::class);
    }

    function it_maps_to_model(ToDo $toDoEntity)
    {
        $toDoEntity->getId()->willReturn(1);
        $toDoEntity->getDescription()->willReturn('A description');
        $toDoEntity->getDueAt()->willReturn(new DateTIme('2018-05-03'));
        $toDoEntity->getUpdatedAt()->willReturn(new DateTime('2018-04-30'));

        $model = $this->createToDo($toDoEntity);

        $model->id->shouldBe(1);
        $model->description->shouldBe('A description');
        $model->dueDate->shouldHaveType(DateTime::class);
        $model->updatedAt->shouldHaveType(DateTime::class);
    }
}
