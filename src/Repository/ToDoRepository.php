<?php

namespace App\Repository;

use App\Entity\ToDo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ToDoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ToDo::class);
    }

    public function findPaginatedUndeleted(int $limit, int $offset)
    {
        $qb = $this->createUndeletedQueryBuilder()
            ->orderBy('tdl.dueAt', 'desc')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery();

        return $qb->execute();
    }

    public function countUndeleted()
    {
        $qb = $this->createUndeletedQueryBuilder()
            ->select('COUNT(tdl)')
            ->getQuery();

        return $qb->getSingleScalarResult();
    }

    private function createUndeletedQueryBuilder()
    {
        return $this->createQueryBuilder('tdl')->where('tdl.deletedAt IS NULL');
    }
}
