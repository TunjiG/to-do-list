<?php

namespace App\Service;

use App\Model\ToDo as ToDoModel;
use App\Exception\ToDoException;
use App\Model\ToDoCollection;

interface ToDoServiceInterface
{
    /**
     * @return ToDoModel
     *
     * @throws ToDoException
     */
    public function get(int $id);

    /**
     * @return int
     *
     * @throws ToDoException
     */
    public function create(ToDoModel $model);

    /**
     * @throws ToDoException
     */
    public function update(ToDoModel $model);

    /**
     * @throws ToDoException
     */
    public function delete(int $id);

    /**
     * @return ToDoCollection
     */
    public function list(int $page);
}
