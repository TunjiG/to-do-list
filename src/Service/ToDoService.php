<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Model\ToDo as ToDoModel;
use App\Entity\ToDo as ToDoEntity;
use App\Exception\ToDoException;
use Exception;
use App\Factory\ModelFactoryInterface;
use App\Factory\EntityFactoryInterface;
use App\Repository\ToDoRepository;
use App\Model\ToDoCollection;

class ToDoService implements ToDoServiceInterface
{
    const PAGE_LIMIT = 5;

    private $entityManager;
    private $toDoRepository;
    private $modelFactory;
    private $entityFactory;

    public function __construct(
        EntityFactoryInterface $entityFactory,
        ModelFactoryInterface $modelFactory,
        ToDoRepository $toDoRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->entityFactory = $entityFactory;
        $this->modelFactory = $modelFactory;
        $this->toDoRepository = $toDoRepository;
        $this->entityManager = $entityManager;
    }

    public function get(int $id)
    {
        try {
            $entity = $this->toDoRepository->find($id);
            if (!$entity instanceof ToDoEntity) {
                throw new Exception();
            }

            return $this->modelFactory->createToDo($entity);
        } catch (Exception $e) {
            throw new ToDoException(sprintf('Could not find an item with id "%d".', $id));
        }
    }

    public function create(ToDoModel $model)
    {
        try {
            $entity = $this->entityFactory->createToDo($model);

            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $entity->getId();
        } catch (Exception $e) {
            throw new ToDoException('Could not create the to-do list item.');
        }
    }

    public function update(ToDoModel $model)
    {
        try {
            $entity = $this->toDoRepository->find($model->id);
            if (!$entity instanceof ToDoEntity) {
                throw new Exception();
            }

            $entity = $this->entityFactory->updateToDo($model, $entity);

            $this->entityManager->flush();
        } catch (Exception $e) {
            throw new ToDoException('Could not update the to-do list item.');
        }
    }

    public function delete(int $id)
    {
        try {
            $entity = $this->toDoRepository->find($id);
            if (!$entity instanceof ToDoEntity) {
                throw new Exception();
            }

            $this->entityManager->remove($entity);

            $this->entityManager->flush();
        } catch (Exception $e) {
            throw new ToDoException('Could not delete the to-do list item.');
        }
    }

    public function list(int $page)
    {
        $total = $this->toDoRepository->countUndeleted();
        $page = $this->normalisePage(
            $page,
            ceil($total / self::PAGE_LIMIT)
        );

        $collection = new ToDoCollection();
        $collection->total = $total;
        $collection->page = $page;
        $collection->limit = self::PAGE_LIMIT;

        $dbResults = $this->toDoRepository->findPaginatedUndeleted(
            self::PAGE_LIMIT,
            $this->getOffset(self::PAGE_LIMIT, $page)
        );

        foreach ($dbResults as $dbResult) {
            $collection->items[] = $this->modelFactory->createToDo($dbResult);
        }

        return $collection;
    }

    private function normalisePage($page, $totalPages)
    {
        if ($page < 1) {
            $page = 1;
        }

        if ($page > $totalPages) {
            $page = $totalPages;
        }

        return $page;
    }

    private function getOffset($limit, $page)
    {
        $offset = 0;
        if ($page > 1) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }
}
