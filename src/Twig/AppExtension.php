<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return array(
            new TwigFunction('calc_pagination_sequence', [$this, 'calculatePaginationSequence']),
        );
    }

    public function calculatePaginationSequence($page, $total, $limit)
    {
        $sequence = [];
        $countAround = 2;
        $totalPages = ceil($total / $limit);
        $currentPage = $page - 1;

        for ($i = 0; $i < $totalPages; ++$i) {
            $isGap = false;

            if (
                $countAround >= 0 &&
                $i > 0 &&
                $i < $totalPages - 1 &&
                abs($i - $currentPage) > $countAround
            ) {
                $isGap = true;
                $i = ($i < $currentPage ? $currentPage - $countAround : $totalPages - 1) - 1;
            }

            if ($isGap) {
                $sequence[] = null;
            } else {
                $sequence[] = $i + 1;
            }
        }

        return $sequence;
    }
}
