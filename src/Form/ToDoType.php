<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type as SymfonyFormType;
use App\Model\ToDo;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ToDoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'description',
                SymfonyFormType\TextType::class,
                [
                  'label' => 'Description',
                  'required' => true,
                  'attr' => ['class' => 'input', 'placeholder' => 'Description'],
                ]
            )
            ->add(
                'dueDate',
                SymfonyFormType\DateTimeType::class,
                [
                  'label' => 'Due date',
                  'required' => true,
                  'attr' => ['class' => 'input', 'placeholder' => 'Select a value'],
                ]
            )
            ->add(
                'submit',
                SymfonyFormType\SubmitType::class,
                [
                    'label' => 'submit',
                    'attr' => ['class' => 'btn-primary'],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ToDo::class,
        ));
    }
}
