<?php

namespace App\Exception;

use RuntimeException;

class ToDoException extends RuntimeException
{
}
