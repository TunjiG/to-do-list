<?php

namespace App\Model;

class ToDoCollection
{
    /**
     * ToDo[].
     */
    public $items = [];

    public $total = 0;

    public $page;

    public $limit;
}
