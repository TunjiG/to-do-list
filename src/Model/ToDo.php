<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ToDo
{
    public $id;

    /**
     * @Assert\NotBlank()
     */
    public $description;

    /**
     * @Assert\NotBlank()
     */
    public $dueDate;

    public $createdAt;

    public $updatedAt;
}
