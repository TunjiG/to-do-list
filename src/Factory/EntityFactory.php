<?php

namespace App\Factory;

use App\Entity\ToDo as ToDoEntity;
use App\Model\ToDo as ToDoModel;
use DateTIme;

class EntityFactory implements EntityFactoryInterface
{
    public function createToDo(ToDoModel $model)
    {
        $entity = new ToDoEntity();

        // Would normally use Gedmo extension to automatically set the
        // the creation & update dates
        $entity
          ->setCreatedAt(new DateTIme())
          ->setUpdatedAt(clone $entity->getCreatedAt())
          ->setDueAt($model->dueDate)
          ->setDescription($model->description);

        return $entity;
    }

    public function updateToDo(ToDoModel $model, ToDoEntity $entity)
    {
        $entity
          ->setUpdatedAt(new DateTIme())
          ->setDueAt($model->dueDate)
          ->setDescription($model->description);

        return $entity;
    }
}
