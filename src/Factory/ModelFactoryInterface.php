<?php

namespace App\Factory;

use App\Entity\ToDo as ToDoEntity;
use App\Model\ToDo as ToDoModel;

interface ModelFactoryInterface
{
    /**
     * @return ToDoModel
     */
    public function createToDo(ToDoEntity $toDoEntity);
}
