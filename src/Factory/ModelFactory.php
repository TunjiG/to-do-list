<?php

namespace App\Factory;

use App\Entity\ToDo as ToDoEntity;
use App\Model\ToDo as ToDoModel;

class ModelFactory implements ModelFactoryInterface
{
    public function createToDo(ToDoEntity $toDoEntity)
    {
        $model = new ToDoModel();

        $model->id = $toDoEntity->getId();
        $model->description = $toDoEntity->getDescription();
        $model->dueDate = $toDoEntity->getDueAt();
        $model->updatedAt = $toDoEntity->getUpdatedAt();

        return $model;
    }
}
