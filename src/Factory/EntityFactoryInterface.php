<?php

namespace App\Factory;

use App\Entity\ToDo as ToDoEntity;
use App\Model\ToDo as ToDoModel;

interface EntityFactoryInterface
{
    /**
     * @return ToDoEntity
     */
    public function createToDo(ToDoModel $model);

    /**
     * @return ToDoEntity
     */
    public function updateToDo(ToDoModel $model, ToDoEntity $entity);
}
