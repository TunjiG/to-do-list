<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use App\Model\ToDo;
use App\Form\ToDoType;
use App\Service\ToDoServiceInterface;
use App\Exception\ToDoException;

/**
 * @Route("/to-do-list")
 */
class ToDoController extends Controller
{
    private $toDoService;

    public function __construct(
        ToDoServiceInterface $toDoService
    ) {
        $this->toDoService = $toDoService;
    }

    /**
     * @Route("/{page}", requirements={"page"="\d+"}, defaults={"page" = 1}, name="to_do_list_index")
     */
    public function index(Request $request, $page)
    {
        return $this->render('ToDo/index.html.twig', ['list' => $this->toDoService->list($page)]);
    }

    /**
     * @Route("/create", name="to_do_list_create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(ToDoType::class, new ToDo());

        $session = $request->getSession();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $id = $this->toDoService->create($form->getData());

                $session->getFlashBag()->add('success', sprintf('You\'ve successfully created item #%d.', $id));

                return $this->redirectToRoute('to_do_list_index');
            } catch (ToDoException $e) {
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('ToDo/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/update/{id}", requirements={"id"="\d+"}, name="to_do_list_update")
     */
    public function update(Request $request, $id)
    {
        $session = $request->getSession();

        try {
            $form = $this->createForm(ToDoType::class, $this->toDoService->get($id));

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->toDoService->update($form->getData());

                $session->getFlashBag()->add('success', sprintf('You\'ve successfully updated item #%d.', $id));

                return $this->redirectToRoute('to_do_list_index');
            }

            return $this->render('ToDo/update.html.twig', [
                'form' => $form->createView(),
            ]);
        } catch (ToDoException $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }

        return $this->redirectToRoute('to_do_list_index');
    }

    /**
     * @Route("/delete/{id}", requirements={"id"="\d+"}, name="to_do_list_delete")
     */
    public function delete(Request $request, $id)
    {
        $session = $request->getSession();

        try {
            $this->toDoService->delete($id);

            $session->getFlashBag()->add('success', sprintf('You\'ve successfully deleted item #%d', $id));
        } catch (ToDoException $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }

        return $this->redirectToRoute('to_do_list_index', ['page' => 1]);
    }
}
